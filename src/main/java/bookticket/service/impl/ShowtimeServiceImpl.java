package bookticket.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bookticket.entities.Showtimes;
import bookticket.repository.ShowtimeRepository;
import bookticket.service.ShowtimeService;

@Service
public class ShowtimeServiceImpl implements ShowtimeService{
	
	@Autowired
	private ShowtimeRepository showtimeRepository;

	@Override
	public Iterable<Showtimes> findAll() {
		return showtimeRepository.findAll();
	}

	@Override
	public Showtimes findOne(int id) {
		return showtimeRepository.findOne(id);
	}

	@Override
	public Showtimes save(Showtimes showtime) {
		return showtimeRepository.save(showtime);
	}

	@Override
	public void delete(int id) {
		showtimeRepository.delete(id);
	}

	@Override
	public List<Showtimes> findByCinema(Integer id) {
		return showtimeRepository.findByCinema(id);
	}

	@Override
	public List<Showtimes> findByCinemaAndMovie(int cinemaId, int movieId) {
		return showtimeRepository.findByCinemaAndMovie(cinemaId, movieId);
	}

}
