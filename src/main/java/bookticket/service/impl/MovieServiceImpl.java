package bookticket.service.impl;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import bookticket.entities.Movie;
import bookticket.repository.MovieRepository;
import bookticket.service.MovieService;

@Service
public class MovieServiceImpl implements MovieService{

	@Autowired
	private MovieRepository movieRepository;
	
	@Override
	public Iterable<Movie> findAll() {
		return movieRepository.findAll();
	}

	@Override
	public Movie findOne(Integer movieId) {
		return movieRepository.findOne(movieId);
	}

	@Override
	public Movie save(Movie movie) {
		return movieRepository.save(movie);
	}

	@Override
	public void delete(Integer movieId) {
		movieRepository.delete(movieId);
	}

	@Override
	public void saveImage(MultipartFile imageFile) throws Exception{
		Path currentPath = Paths.get(".");
		Path absolutePath = currentPath.toAbsolutePath();
		
		String filePath = absolutePath + "/src/main/resources/static/img/";
		/* File file = new File(photoDTO.getPath()); */
		/*
		 * if(!file.exists()) { file.mkdirs(); }
		 */
		byte[] bytes = imageFile.getBytes();
		Path path = Paths.get(filePath + imageFile.getOriginalFilename());
		Files.write(path, bytes);
	}

}
