package bookticket.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bookticket.entities.ScreeningRoom;
import bookticket.entities.Seat;
import bookticket.repository.ScreeningRoomRepository;
import bookticket.repository.SeatRepository;
import bookticket.service.ScreeningRoomService;

@Service
public class ScreeningRoomServiceImpl implements ScreeningRoomService{

	@Autowired
	private ScreeningRoomRepository sRoomRepository;
	
	@Autowired SeatRepository seatRepository;
	
	@Override
	public Iterable<ScreeningRoom> findAll() {
		return sRoomRepository.findAll();
	}

	@Override
	public ScreeningRoom findOne(int id) {
		return sRoomRepository.findOne(id);
	}

	@Override
	public ScreeningRoom save(ScreeningRoom sRoom) {
		return sRoomRepository.save(sRoom);
	}

	@Override
	public void delete(int id) {
		sRoomRepository.delete(id);
	}

	@Override
	public List<Seat> findByRoom(ScreeningRoom room) {
		return seatRepository.findByRoom(room);
	}

	@Override
	public List<ScreeningRoom> findByCinema(Integer user_id) {
		return sRoomRepository.findByCinema(user_id);
	}

}
