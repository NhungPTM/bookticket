package bookticket.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bookticket.entities.Seat;
import bookticket.repository.SeatRepository;
import bookticket.service.SeatService;

@Service
public class SeatServiveImpl implements SeatService{

	@Autowired
	private SeatRepository seatRepository;
	
	@Override
	public Iterable<Seat> findAll() {
		return seatRepository.findAll();
	}

	@Override
	public Seat findOne(int id) {
		return seatRepository.findOne(id);
	}

	@Override
	public Seat save(Seat seat) {
		return seatRepository.save(seat);
	}

	@Override
	public void delete(int id) {
		seatRepository.delete(id);
	}

}
