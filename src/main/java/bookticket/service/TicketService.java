package bookticket.service;

import bookticket.entities.Ticket;

public interface TicketService {
	Iterable<Ticket> findAll();
	
	Ticket findOne(int id);
	
	Ticket save (Ticket ticket);
	
	void delete(int id);
}
