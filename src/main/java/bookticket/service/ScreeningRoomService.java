package bookticket.service;

import java.util.List;

import bookticket.entities.ScreeningRoom;
import bookticket.entities.Seat;

public interface ScreeningRoomService {
	Iterable<ScreeningRoom> findAll();
	
	ScreeningRoom findOne(int id);
	
	ScreeningRoom save(ScreeningRoom sRoom);
	
	void delete(int id);
	
	List<Seat> findByRoom(ScreeningRoom room);
	
	List<ScreeningRoom> findByCinema(Integer user_id);
}
