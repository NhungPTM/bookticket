package bookticket.service;

import java.util.List;

import bookticket.entities.Showtimes;

public interface ShowtimeService {
	Iterable<Showtimes> findAll();
	
	Showtimes findOne(int id);
	
	Showtimes save(Showtimes showtime);
	
	void delete(int id);
	
	List<Showtimes> findByCinema(Integer id);
	
	List<Showtimes> findByCinemaAndMovie(int cinemaId, int movieId);
}
