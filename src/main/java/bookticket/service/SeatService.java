package bookticket.service;

import bookticket.entities.Seat;

public interface SeatService {
	Iterable<Seat> findAll();
	
	Seat findOne(int id);
	
	Seat save(Seat seat);
	
	void delete(int id);
}
