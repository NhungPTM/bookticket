package bookticket.service;

import java.util.List;

import bookticket.entities.User;

public interface UserService {
	Iterable<User> findAll();
	
	User findOne(int id);
	
	User save(User user);
	
	void delete(int id);
	
	User findByUserName(String userName);
	
	User findByEmail(String email);
	
	User findByPhone(String phone);
	
	List<User> findByRole(Integer id);
}
