package bookticket.service;

import org.springframework.web.multipart.MultipartFile;

import bookticket.entities.Movie;

public interface MovieService {
	Iterable<Movie> findAll();
	
	Movie findOne(Integer movieId);
	
	Movie save(Movie movie);
	
	void delete(Integer movieId);
	
	void saveImage(MultipartFile imageFile) throws Exception;
}
