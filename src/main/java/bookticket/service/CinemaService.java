package bookticket.service;

import org.springframework.web.multipart.MultipartFile;

import bookticket.entities.Cinema;
import bookticket.entities.User;

public interface CinemaService {
	Iterable<Cinema> findAll();
	
	Cinema findOne(int id);
	
	Cinema save(Cinema cinema);
	
	void delete(int id);

	Cinema findByUser(User user);
	
	void saveImage(MultipartFile imageFile) throws Exception;
}
