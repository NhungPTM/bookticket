package bookticket;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@Controller
public class BookTicketApplication {
	@GetMapping("/home")
	String home() {
		return "Home";
	}
	
	@GetMapping("/signUp")
	String signUp() {
		
		return "SignUp";
	}
	
	public static void main(String[] args) {
		SpringApplication.run(BookTicketApplication.class, args);
	}
}
