package bookticket.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "movie")
public class Movie implements Serializable{

	private static final long serialVersionUID = 7501251046768162896L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "movie_id")
	private Integer movieId;
	
	@Column(name = "movie_name")
	private String movieName;
	
	@Column(name = "image")
	private String image;
	
	@Column(name = "start_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	private Date startDate;
	
	@Column(name = "end_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	private Date endDate;
	
	@Column(name = "description")
	private String desciption;
	
	@Column(name = "trailer")
	private String trailer;
	
	@Column(name = "time")
	private Integer time;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "actor")
	private String actor;
	
	@Column(name = "director")
	private String director;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="country_id")
	private Country country;
			
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="category_id")
	private Category category;
	
	@OneToMany(mappedBy="movie")
	private Set<Showtimes> showtimes;
	
	public Movie() {
	}

	public Movie(Integer movieId, String movieName, String image, Date startDate, Date endDate, String desciption,
			String trailer, Integer time, String status, String actor, String director, Country country,
			Category category) {
		super();
		this.movieId = movieId;
		this.movieName = movieName;
		this.image = image;
		this.startDate = startDate;
		this.endDate = endDate;
		this.desciption = desciption;
		this.trailer = trailer;
		this.time = time;
		this.status = status;
		this.actor = actor;
		this.director = director;
		this.country = country;
		this.category = category;
	}

	public Integer getMovieId() {
		return movieId;
	}

	public void setMovieId(Integer movieId) {
		this.movieId = movieId;
	}

	public String getMovieName() {
		return movieName;
	}

	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getDesciption() {
		return desciption;
	}

	public void setDesciption(String desciption) {
		this.desciption = desciption;
	}

	public String getTrailer() {
		return trailer;
	}

	public void setTrailer(String trailer) {
		this.trailer = trailer;
	}

	public Integer getTime() {
		return time;
	}

	public void setTime(Integer time) {
		this.time = time;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getActor() {
		return actor;
	}

	public void setActor(String actor) {
		this.actor = actor;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		
		if(!(obj instanceof Movie)) {
			return false;
		}
		
		Movie movie = (Movie) obj;
		return Objects.equals(this.getMovieId(), movie.getMovieId());
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(this.movieId);
	}
}
