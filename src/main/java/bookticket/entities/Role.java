package bookticket.entities;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import bookticket.entities.User;

@Entity
@Table(name = "roles")
public class Role implements Serializable{

	private static final long serialVersionUID = -2295169704233718959L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "role_id")
    private Integer id;
    
    @Column(name = "role_name")
    private String role;
    
    @ManyToMany(mappedBy = "roles")
    private Set<User> users;
    
    public Role() {
	}

	public Role(int id, String role, Set<User> users) {
		super();
		this.id = id;
		this.role = role;
		this.users = users;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}
    
    @Override
    public boolean equals(Object obj) {
    	if(this == obj) {
    		return true;
    	}
    	
    	if(!(obj instanceof Role)) {
    		return false;
    	}
    	
    	Role role = (Role) obj;
    	
    	return (role.getId() == this.getId());
    }
    
    @Override
    public int hashCode() {
    	return Objects.hash(this.getId());
    }
}