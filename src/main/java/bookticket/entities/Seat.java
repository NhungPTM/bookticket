package bookticket.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "seat")
public class Seat implements Serializable{
	
	private static final long serialVersionUID = -1451371778108983002L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "seat_id")
	private Integer seatId;
	
	@Column(name = "seat_name")
	private String seatName;
	
	@Column(name = "status")
	private int status;
	
	@ManyToOne
	@JoinColumn(name="room_id")
	private ScreeningRoom room;

	public Seat() {
	}
	
	public Seat(Integer seatId, String seatName, int status, ScreeningRoom room) {
		super();
		this.seatId = seatId;
		this.seatName = seatName;
		this.status = status;
		this.room = room;
	}

	public Integer getSeatId() {
		return seatId;
	}

	public void setSeatId(Integer seatId) {
		this.seatId = seatId;
	}

	public String getSeatName() {
		return seatName;
	}

	public void setSeatName(String seatName) {
		this.seatName = seatName;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public ScreeningRoom getRoom() {
		return room;
	}

	public void setRoom(ScreeningRoom room) {
		this.room = room;
	}

}
