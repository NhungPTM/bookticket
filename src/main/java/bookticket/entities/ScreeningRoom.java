package bookticket.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "screening_room")
public class ScreeningRoom implements Serializable{
	
	private static final long serialVersionUID = -6880604610559033630L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "room_id")
	private int roomId;
	
	@Column(name = "room_name")
	
	private String roomName;
	
	@Column(name = "seats")
	private int seats;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cinema_id")
	private Cinema cinema;
	
	@OneToMany(mappedBy="room")
	private List<Seat> seatlist;
	
	@OneToMany(mappedBy="room")
	private List<Showtimes> showtimes;
	
	public ScreeningRoom() {
	}

	public ScreeningRoom(int roomId, String roomName, int seats, List<Seat> seatlist) {
		super();
		this.roomId = roomId;
		this.roomName = roomName;
		this.seats = seats;
		this.seatlist = seatlist;
	}

	public int getRoomId() {
		return roomId;
	}

	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		this.seats = seats;
	}

	public List<Seat> getSeatlist() {
		return seatlist;
	}

	public void setSeatlist(List<Seat> seatlist) {
		this.seatlist = seatlist;
	}

	public Cinema getCinema() {
		return cinema;
	}

	public void setCinema(Cinema cinema) {
		this.cinema = cinema;
	}

	public List<Showtimes> getShowtimes() {
		return showtimes;
	}

	public void setShowtimes(List<Showtimes> showtimes) {
		this.showtimes = showtimes;
	}
	
}
