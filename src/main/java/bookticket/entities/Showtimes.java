package bookticket.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "showtime")
public class Showtimes implements Serializable{

	private static final long serialVersionUID = 7267740745952073421L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "showtime_id")
	private Integer showtimeId;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name = "showtime_date")
	private Date date;
	
	@DateTimeFormat(pattern = "HH:mm")
	@Temporal(TemporalType.TIME)
	@Column(name = "start_time")
	private Date startTime;
	
	@DateTimeFormat(pattern = "HH:mm")
	@Temporal(TemporalType.TIME)
	@Column(name = "end_time")
	private Date endTime;
	
	@Column(name = "empty_seat_number")
	private int emptySeats;
	
	@Column(name = "price")
	private double price;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="room_id")
	private ScreeningRoom room;
			
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="movie_id")
	private Movie movie;
	
	@OneToMany(mappedBy="showtime")
	private List<Ticket> tickets;
	
	public Showtimes() {
	}

	public Showtimes(Integer showtimeId, Date date, Date startTime, Date endTime, int emptySeats, double price,
			ScreeningRoom room, Movie movie) {
		super();
		this.showtimeId = showtimeId;
		this.date = date;
		this.startTime = startTime;
		this.endTime = endTime;
		this.emptySeats = emptySeats;
		this.price = price;
		this.room = room;
		this.movie = movie;
	}



	public Integer getShowtimeId() {
		return showtimeId;
	}

	public void setShowtimeId(Integer showtimeId) {
		this.showtimeId = showtimeId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public int getEmptySeats() {
		return emptySeats;
	}

	public void setEmptySeats(int emptySeats) {
		this.emptySeats = emptySeats;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public ScreeningRoom getRoom() {
		return room;
	}

	public void setRoom(ScreeningRoom room) {
		this.room = room;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public List<Ticket> getTickets() {
		return tickets;
	}

	public void setTickets(List<Ticket> tickets) {
		this.tickets = tickets;
	}
	
	
}
