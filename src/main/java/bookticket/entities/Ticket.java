package bookticket.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "ticket")
public class Ticket implements Serializable{

	private static final long serialVersionUID = -8751440143961625347L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ticket_id")
	private Integer ticketId;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "booked_date")
	private Date bookedDate;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "count_ticket")
	private int countTicket;
	
	@Column(name = "total")
	private double total;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id")
	private User user;
	
	@ManyToMany
    @JoinTable(name = "ticket_seat", joinColumns = @JoinColumn(name = "ticket_id"), inverseJoinColumns = @JoinColumn(name = "seat_id"))
	private Set<Seat> seats;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="showtime_id")
	private Showtimes showtime;
	
	public Ticket() {
	}

	public Ticket(Integer ticketId, Date bookedDate, String status, int countTicket, double total, User user,
			Set<Seat> seats, Showtimes showtime) {
		this.ticketId = ticketId;
		this.bookedDate = bookedDate;
		this.status = status;
		this.countTicket = countTicket;
		this.total = total;
		this.user = user;
		this.seats = seats;
		this.showtime = showtime;
	}

	public Integer getTicketId() {
		return ticketId;
	}

	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	public Date getBookedDate() {
		return bookedDate;
	}

	public void setBookedDate(Date bookedDate) {
		this.bookedDate = bookedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getCountTicket() {
		return countTicket;
	}

	public void setCountTicket(int countTicket) {
		this.countTicket = countTicket;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<Seat> getSeats() {
		return seats;
	}

	public void setSeats(Set<Seat> seats) {
		this.seats = seats;
	}

	public Showtimes getShowtime() {
		return showtime;
	}

	public void setShowtime(Showtimes showtime) {
		this.showtime = showtime;
	}
		
}
