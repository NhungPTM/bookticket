package bookticket.controller;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import bookticket.entities.Role;
import bookticket.entities.User;
import bookticket.repository.RoleRepository;
import bookticket.service.UserService;

@Controller
public class AdminUserController {
	@Autowired
	private UserService userService;

	@Autowired
	private RoleRepository roleRepository;

	@GetMapping("/he-thong/nguoi-dung")
	public ModelAndView getListUser(ModelAndView mv) {
		mv.addObject("userlist", userService.findAll());
		mv.setViewName("AdminListUser");
		return mv;
	}
	
	@GetMapping("/he-thong/quan-ly-rap")
	public ModelAndView getListAdmin(ModelAndView mv) {
		Role role = roleRepository.findByRole("ADMIN");
		Integer id = role.getId();
		mv.addObject("adminlist", userService.findByRole(id));
		mv.setViewName("AdminListAdmin");
		return mv;
	}


	@GetMapping("/he-thong/them-quan-ly-rap")
	public ModelAndView addAdmin(ModelAndView mv) {
		mv.addObject("admin", new User());
		mv.setViewName("AdminAddUser");
		return mv;
	}

	@PostMapping("/he-thong/luu-quan-ly-rap")
	public ModelAndView saveInformation(@Valid @ModelAttribute("admin") User admin, BindingResult result) {
		ModelAndView mv = new ModelAndView();

		User userNameExists = userService.findByUserName(admin.getUserName());
		if (userNameExists != null) {
			result.rejectValue("userName", "error.user", "Duplicate user's username.");
		}

		if (result.hasErrors()) {
			mv.setViewName("AdminAddUser");
			return mv;
		} else {
			Role userRole = roleRepository.findByRole("ADMIN");
			admin.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
			admin.setActive(true);
			userService.save(admin);
			mv.addObject("success", "Lưu dữ liệu thành công");
			mv.setViewName("AdminListAdmin");
		}
		return mv;
	}
}
