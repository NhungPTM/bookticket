package bookticket.controller;

import java.util.Arrays;
import java.util.HashSet;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import bookticket.entities.ConfirmationToken;
import bookticket.entities.Role;
import bookticket.entities.User;
import bookticket.repository.ConfirmationTokenRepository;
import bookticket.repository.RoleRepository;
import bookticket.service.UserService;
import bookticket.service.impl.EmailSenderService;

@Controller
public class RegistrationController {

	@Autowired
	private UserService userService;

	@Autowired
	private EmailSenderService emailSenderService;

	@Autowired
	private ConfirmationTokenRepository confirmationTokenRepository;

	@Autowired
	private RoleRepository roleRepository;

	@GetMapping("/dang-ky")
	public ModelAndView addUser(ModelAndView mv) {
		mv.addObject("user", new User());
		mv.setViewName("SignUp");
		return mv;
	}

	@PostMapping("/dang-ky")
	public ModelAndView confirm(@Valid User user, BindingResult result) {
		ModelAndView mv = new ModelAndView();

		User userNameExists = userService.findByUserName(user.getUserName());
		if (userNameExists != null) {
			result.rejectValue("userName", "error.user", "Duplicate user's username.");
		}

		User userEmailExists = userService.findByEmail(user.getEmail());
		if (userEmailExists != null) {
			result.rejectValue("email", "error.user", "Duplicate user's email.");
		}

		User userPhoneExists = userService.findByPhone(user.getPhone());
		if (userPhoneExists != null) {
			result.rejectValue("phone", "error.user", "Duplicate user's phone.");
		}

		/*
		 * if(result.hasErrors()) { mv.setViewName("SignUp"); }else {
		 */
		Role userRole = roleRepository.findByRole("USER");
		user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
		userService.save(user);

		ConfirmationToken confirmationToken = new ConfirmationToken(user);

		confirmationTokenRepository.save(confirmationToken);

		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(user.getEmail());
		mailMessage.setSubject("Chúc mừng bạn đăng ký thành công!");
		mailMessage.setText("Để xác nhận tài khoản, vui lòng nhấn vào đây: "
				+ "http://localhost:8080/confirm-account?token=" + confirmationToken.getConfirmationToken());

		emailSenderService.sendEmail(mailMessage);

		mv.addObject("emailId", user.getEmail());

		mv.setViewName("successfulRegisteration");

		return mv;
	}

	@PostMapping("/user/save")
	public String saveUser(@Valid User user, BindingResult result, RedirectAttributes redirect) {
		if (result.hasErrors()) {
			return "AddUser";
		}
		User userNameExists = userService.findByUserName(user.getUserName());
		if (userNameExists != null) {
			result.rejectValue("userName", "error.user", "Duplicate user's username.");
		}

		User userEmailExists = userService.findByEmail(user.getEmail());
		if (userEmailExists != null) {
			result.rejectValue("email", "error.user", "Duplicate user's email.");
		}

		User userPhoneExists = userService.findByPhone(user.getPhone());
		if (userPhoneExists != null) {
			result.rejectValue("phone", "error.user", "Duplicate user's phone.");
		}
		userService.save(user);
		redirect.addFlashAttribute("success", "Lưu dữ liệu thành công!");
		return "redirect:/user";
	}

	@RequestMapping(value = "/confirm-account", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView confirmUserAccount(ModelAndView mv, @RequestParam("token") String confirmationToken) {
		ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);

		if (token != null) {
			User user = userService.findByEmail(token.getUser().getEmail());
			user.setActive(true);
			userService.save(user);
			mv.setViewName("accountVerified");
		} else {
			mv.addObject("message", "The link is invalid or broken!");
			mv.setViewName("error");
		}

		return mv;
	}
}
