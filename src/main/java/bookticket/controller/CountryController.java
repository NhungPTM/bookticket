package bookticket.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import bookticket.entities.Country;
import bookticket.service.CountryService;

@Controller
public class CountryController {
	@Autowired
	private CountryService countryService;
	
	@GetMapping("/he-thong/quoc-gia")
	public ModelAndView listCountry(ModelAndView mv) {
		mv.addObject("countrylist", countryService.findAll());
		mv.setViewName("AdminListCountry");
		return mv;
	}
	
	@GetMapping("/he-thong/them-quoc-gia")
	public ModelAndView addCountry(ModelAndView mv) {
		mv.addObject("country", new Country());
		mv.setViewName("AdminAddCountry");
		return mv;
	}
	
	@PostMapping("/he-thong/luu-quoc-gia")
	public String saveCountry(@Valid Country country, BindingResult result, RedirectAttributes redirect) {
		if(result.hasErrors()) {
			return "AdminAddCountry";
		}
		countryService.save(country);
		redirect.addFlashAttribute("success", "Lưu dữ liệu thành công!");
		return "redirect:/he-thong/quoc-gia";
	}
	
	@GetMapping("/he-thong/quoc-gia/{id}/sua")
	public ModelAndView edit(@PathVariable("id") int countryId, ModelAndView mv) {
		mv.addObject("country", countryService.findOne(countryId));
		mv.setViewName("AdminAddCountry");
		return mv;
	}
	
	@GetMapping("/he-thong/quoc-gia/{id}/xoa")
	public String delete(@PathVariable("id") int countryId, RedirectAttributes redirect) {
		countryService.delete(countryId);
		redirect.addFlashAttribute("success", "Xóa thành công");
		return "redirect:/he-thong/quoc-gia";
	}
}
