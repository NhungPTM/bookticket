package bookticket.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import bookticket.entities.Category;
import bookticket.service.CategoryService;

@Controller
public class CategoryController {
	
	@Autowired
	private CategoryService categoryService;
	
	@GetMapping("/he-thong/the-loai")
	public ModelAndView listCategory(ModelAndView mv) {
		mv.addObject("categorylist", categoryService.findAll());
		mv.setViewName("AdminListCategory");
		return mv;
	}
	
	@GetMapping("/he-thong/the-loai/them-the-loai")
	public ModelAndView addCategory(ModelAndView mv) {
		mv.addObject("category", new Category());
		mv.setViewName("AdminAddCategory");
		return mv;
	}
	
	@PostMapping("/he-thong/the-loai/luu-the-loai")
	public String saveCategory(@Valid Category category, BindingResult result, RedirectAttributes redirect) {
		if(result.hasErrors()) {
			return "AdminAddCategory";
		}
		categoryService.save(category);
		redirect.addFlashAttribute("success", "Lưu dữ liệu thành công!");
		return "redirect:/he-thong/the-loai";
	}
	
	@GetMapping("/he-thong/the-loai/{id}/sua-the-loai")
	public ModelAndView edit(@PathVariable("id") int categoryId, ModelAndView mv) {
		mv.addObject("category", categoryService.findOne(categoryId));
		mv.setViewName("AdminAddCategory");
		return mv;
	}
	
	@GetMapping("/he-thong/the-loai/{id}/xoa-the-loai")
	public String delete(@PathVariable("id") int categoryId, RedirectAttributes redirect) {
		categoryService.delete(categoryId);
		redirect.addFlashAttribute("success", "Xóa thành công");
		return "redirect:/he-thong/the-loai";
	}
}
