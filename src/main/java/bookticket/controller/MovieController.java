package bookticket.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import bookticket.entities.Movie;
import bookticket.service.CategoryService;
import bookticket.service.CountryService;
import bookticket.service.MovieService;

@Controller
public class MovieController {
	@Autowired
	private MovieService movieService;
	
	@Autowired
	private CountryService countryService;
	
	@Autowired
	private CategoryService categoryService;
	
	@GetMapping("/he-thong/phim")
	public ModelAndView roomList(ModelAndView mv) {
		mv.addObject("movielist", movieService.findAll());
		mv.setViewName("AdminListMovie");
		return mv;
	}
	
	@GetMapping(value = "/he-thong/phim/them-phim")
	public ModelAndView addMovie(ModelAndView mv) {
		mv.addObject("countrylist", countryService.findAll());
		mv.addObject("categorylist", categoryService.findAll());
		mv.addObject("movie", new Movie());
		mv.setViewName("AdminAddMovie");
		return mv;
	}
	
	@PostMapping("/he-thong/phim/luu-phim")
	public String save(@Valid @ModelAttribute("movie") Movie movie,BindingResult result, @RequestParam("imageFile") MultipartFile imageFile, RedirectAttributes redirect) {
		/*
		 * if(result.hasErrors()) { mv.setViewName("AddMovie"); return mv; }
		 */

		try {
			movieService.saveImage(imageFile);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		movie.setImage("/img/"+imageFile.getOriginalFilename());
		System.out.println(movie.getImage());
		
		
		movieService.save(movie);
		redirect.addFlashAttribute("success", "Lưu dữ liệu thành công!");
		return "redirect:/he-thong/phim";
	}
	
	@GetMapping("/he-thong/phim/{id}/sua-phim")
	public ModelAndView edit(@PathVariable("id") int movieId, ModelAndView mv) {
		mv.addObject("countrylist", countryService.findAll());
		mv.addObject("categorylist", categoryService.findAll());
		mv.addObject("movie", movieService.findOne(movieId));
		mv.setViewName("AdminAddMovie");
		return mv;
	}
	
	@GetMapping("/he-thong/phim/{id}/xoa-phim")
	public String delete(@PathVariable("id") int movieId, RedirectAttributes redirect) {
		movieService.delete(movieId);
		redirect.addFlashAttribute("success", "Xóa thành công");
		return "redirect:/he-thong/phim";
	}
}
