package bookticket.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import bookticket.entities.Showtimes;
import bookticket.entities.User;
import bookticket.service.CinemaService;
import bookticket.service.MovieService;
import bookticket.service.ShowtimeService;
import bookticket.service.UserService;

@Controller
public class HomeController {
	@Autowired
	private CinemaService cinemaService;
	
	@Autowired
	private MovieService movieService;
	
	@Autowired
	private ShowtimeService showtimeService;
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/trang-chu")
	public ModelAndView listCinema(ModelAndView mv) {
		mv.addObject("cinemalist", cinemaService.findAll());
		mv.setViewName("Cinema");
		return mv;
	}

	@GetMapping("/rap-chieu-phim")
	public ModelAndView showCinema(ModelAndView mv) {
		mv.addObject("cinemalist", cinemaService.findAll());
		mv.setViewName("Cinema");
		return mv;
	}
	
	@GetMapping("/phim")
	public ModelAndView showMovie(ModelAndView mv) {
		mv.addObject("movielist", movieService.findAll());
		mv.setViewName("Movie");
		return mv;
	}
	
	@GetMapping(value = {"/goc-dien-anh/**", "/chuong-trinh-thanh-vien"})
	public ModelAndView noInfomation(ModelAndView mv) {
		mv.setViewName("NoInformation");
		return mv;
	}
	
	@GetMapping(value = "/xem-lich-chieu")
	public ModelAndView showtime(ModelAndView mv) {
		mv.addObject("cinemalist", cinemaService.findAll());
		mv.addObject("movielist", movieService.findAll());
		mv.setViewName("Showtime");
		return mv;
	}
	
	@GetMapping("/chi-tiet-lich-chieu")
	@ResponseBody
	public ModelAndView showtimeInformation(@RequestParam(name = "cinemaId") int cinemaId, @RequestParam int movieId, ModelAndView mv) {
		mv.addObject("movie", cinemaService.findOne(cinemaId));
		System.out.println(cinemaId);
		mv.addObject("movie", movieService.findOne(movieId));
		mv.addObject("showtimelist", showtimeService.findByCinemaAndMovie(cinemaId, movieId));
		mv.setViewName("ShowtimeDetail");
		return mv;
	}
	

}
