package bookticket.controller;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import bookticket.entities.Showtimes;
import bookticket.entities.User;
import bookticket.service.MovieService;
import bookticket.service.ScreeningRoomService;
import bookticket.service.SeatService;
import bookticket.service.ShowtimeService;
import bookticket.service.UserService;

@Controller
public class ShowtimeController {

	@Autowired
	private ShowtimeService showtimeService;

	@Autowired
	SeatService seatService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ScreeningRoomService roomService;
	
	@Autowired
	private MovieService movieService;
	
	@GetMapping("/rap/lich-chieu")
	public ModelAndView roomList(ModelAndView mv, Principal principal) {
		String userName = principal.getName();
		User user = userService.findByUserName(userName);
		mv.addObject("user", user);
		mv.addObject("showtimelist", showtimeService.findByCinema(user.getUserId()));
		mv.setViewName("ListShowtime");
		return mv;
	}

	@GetMapping("/rap/lich-chieu/them-lich-chieu")
	public ModelAndView addRoom(ModelAndView mv, Principal principal) {
		String userName = principal.getName();
		User user = userService.findByUserName(userName);
		mv.addObject("user", user);
		mv.addObject("roomlist", roomService.findByCinema(user.getUserId()));
		mv.addObject("movielist", movieService.findAll());
		mv.addObject("showtime", new Showtimes());
		mv.setViewName("AddShowtime");
		return mv;
	}

	@PostMapping("/rap/lich-chieu/luu-lich-chieu")
	public String saveScreeningRoom(@Valid @ModelAttribute("showtime") Showtimes showtime, BindingResult result,
			RedirectAttributes redirect, Principal principal, Model model) {
		String userName = principal.getName();
		User user = userService.findByUserName(userName);
		model.addAttribute("user", user);
		if(showtime.getEmptySeats() == 0) {
			showtime.setEmptySeats(showtime.getRoom().getSeats());
		}
		if (result.hasErrors()) {
			return "AddShowtime";
		}
		showtimeService.save(showtime);
		redirect.addFlashAttribute("success", "Lưu dữ liệu thành công!");
		return "redirect:/rap/lich-chieu";
	}

	@GetMapping("/rap/lich-chieu/{id}/sua-lich-chieu")
	public ModelAndView edit(@PathVariable("id") int showtimeId, ModelAndView mv, Principal principal) {
		String userName = principal.getName();
		User user = userService.findByUserName(userName);
		mv.addObject("user", user);
		mv.addObject("roomlist", roomService.findByCinema(user.getUserId()));
		mv.addObject("movielist", movieService.findAll());
		mv.addObject("showtime", showtimeService.findOne(showtimeId));
		mv.setViewName("AddShowtime");
		return mv;
	}

	@GetMapping("/rap/lich-chieu/{id}/xoa-lich-chieu")
	public String delete(@PathVariable("id") int showtimeId, RedirectAttributes redirect, Model model, Principal principal) {
		String userName = principal.getName();
		User user = userService.findByUserName(userName);
		model.addAttribute("user", user);
		showtimeService.delete(showtimeId);
		redirect.addFlashAttribute("success", "Xóa thành công");
		return "redirect:/rap/lich-chieu";
	}
}
