package bookticket.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import bookticket.entities.Showtimes;
import bookticket.entities.User;
import bookticket.repository.ShowtimeRepository;
import bookticket.service.ScreeningRoomService;
import bookticket.service.SeatService;
import bookticket.service.ShowtimeService;
import bookticket.service.UserService;

@Controller
public class BookingTicketController {
	
	@Autowired
	private ScreeningRoomService roomService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ShowtimeService showtimeService;
		
	// show seats
	@GetMapping("/dat-ve")
	public ModelAndView booking(ModelAndView mv) {
		mv.addObject("seats", roomService.findByRoom(roomService.findOne(1)));
		mv.addObject("alphabet", createAlphabet());
		mv.setViewName("view");
		return mv;
	}
	
	private char[] createAlphabet() {
		char alphabet[] = new char[26];
		int k = 0;
		for(int i = 65; i <= 90; i++) {
			alphabet[k] = (char)i;
			k++;
		}
		return alphabet;
	}
	
	@GetMapping("/dat-ve/{id}/chon-cho")
	@ResponseBody
	public ModelAndView edit(@PathVariable("id") int showtimeId, ModelAndView mv, Principal principal) {
		/*
		 * String userName = principal.getName(); User user =
		 * userService.findByUserName(userName); mv.addObject("user", user);
		 */
		System.out.println("aaaaa");
		Showtimes showtime = showtimeService.findOne(showtimeId);
		System.out.println(showtimeId);
		mv.addObject("showtime", showtime);

		mv.addObject("seats", roomService.findByRoom(roomService.findOne(showtime.getRoom().getRoomId())));
		mv.addObject("alphabet", createAlphabet());
		mv.setViewName("view");
		return mv;
	}
}
