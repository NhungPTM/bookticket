package bookticket.controller;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import bookticket.entities.Cinema;
import bookticket.entities.Role;
import bookticket.entities.User;
import bookticket.repository.RoleRepository;
import bookticket.service.CinemaService;
import bookticket.service.UserService;

@Controller
public class AdminCinemaController {
	@Autowired
	private CinemaService cinemaService;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private UserService userService;

	@GetMapping("/he-thong/rap")
	public ModelAndView listCinema(ModelAndView mv, Principal principal) {
		String userName = principal.getName();
		User user = userService.findByUserName(userName);
		mv.addObject("user", user);
		mv.addObject("cinemalist", cinemaService.findAll());
		mv.setViewName("AdminListCinema");
		return mv;
	}

	@GetMapping("/he-thong/them-rap")
	public ModelAndView addCinema(ModelAndView mv, Principal principal) {
		Role role = roleRepository.findByRole("ADMIN");
		Integer id = role.getId();
		mv.addObject("adminlist", userService.findByRole(id));
		String userName = principal.getName();
		User user = userService.findByUserName(userName);
		mv.addObject("user", user);
		mv.addObject("cinema", new Cinema());
		mv.setViewName("AdminAddCinema");
		return mv;
	}

	@PostMapping("/he-thong/luu-rap")
	public String saveCinema(@Valid Cinema cinema, BindingResult result,
			@RequestParam("imageFile") MultipartFile imageFile, RedirectAttributes redirect, Model model,
			Principal principal) {
		String userName = principal.getName();
		User user = userService.findByUserName(userName);
		model.addAttribute("user", user);
		try {
			cinemaService.saveImage(imageFile);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cinema.setCinemaImage("/images/" + imageFile.getOriginalFilename());
		System.out.println(cinema.getCinemaImage());

		cinemaService.save(cinema);
		redirect.addFlashAttribute("success", "Lưu dữ liệu thành công!");
		return "redirect:/he-thong/danh-sach-cac-rap";
	}

	@GetMapping("/he-thong/danh-sach-cac-rap/{id}/sua")
	public ModelAndView edit(@PathVariable("id") int cinemaId, ModelAndView mv, Principal principal) {
		String userName = principal.getName();
		User user = userService.findByUserName(userName);
		mv.addObject("user", user);
		
		Role role = roleRepository.findByRole("ADMIN");
		Integer id = role.getId();
		mv.addObject("adminlist", userService.findByRole(id));
		mv.addObject("cinema", cinemaService.findOne(cinemaId));
		mv.setViewName("AdminAddCinema");
		return mv;
	}

	@GetMapping("/he-thong/danh-sach-cac-rap/{id}/xoa")
	public String delete(@PathVariable("id") int cinemaId, RedirectAttributes redirect, Model model,
			Principal principal) {
		String userName = principal.getName();
		User user = userService.findByUserName(userName);
		model.addAttribute("user", user);
		cinemaService.delete(cinemaId);
		redirect.addFlashAttribute("success", "Xóa thành công");
		return "redirect:/cinema";
	}
}
