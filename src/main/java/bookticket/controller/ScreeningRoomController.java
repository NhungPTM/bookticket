package bookticket.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import bookticket.entities.ScreeningRoom;
import bookticket.entities.Seat;
import bookticket.entities.User;
import bookticket.service.CinemaService;
import bookticket.service.ScreeningRoomService;
import bookticket.service.SeatService;
import bookticket.service.UserService;

@Controller
public class ScreeningRoomController {

	@Autowired
	private ScreeningRoomService roomService;

	@Autowired
	SeatService seatService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private CinemaService cinemaService;

	@GetMapping("/rap/phong-chieu")
	public ModelAndView roomList(ModelAndView mv, Principal principal) {
		String userName = principal.getName();
		User user = userService.findByUserName(userName);
		mv.addObject("user", user);
		mv.addObject("roomlist", roomService.findByCinema(user.getUserId()));
		mv.setViewName("ListAuditorium");
		return mv;
	}

	@GetMapping("/rap/phong-chieu/them-phong-chieu")
	public ModelAndView addRoom(ModelAndView mv, Principal principal) {
		String userName = principal.getName();
		User user = userService.findByUserName(userName);
		mv.addObject("user", user);
		mv.addObject("room", new ScreeningRoom());
		mv.setViewName("AddAuditorium");
		return mv;
	}

	@PostMapping("/rap/phong-chieu/luu-phong-chieu")
	public String saveScreeningRoom(@Valid @ModelAttribute("room") ScreeningRoom room, BindingResult result,
			RedirectAttributes redirect, Principal principal) {
		String userName = principal.getName();
		User user = userService.findByUserName(userName);
		room.setCinema(cinemaService.findByUser(user));
		if (result.hasErrors()) {
			return "AddAuditorium";
		}
		roomService.save(room);
		if (roomService.findByRoom(room).size() == 0) {
			List<String> listSeatName = createSeatName(room.getSeats());
			for (String name : listSeatName) {
				Seat seat = new Seat();
				seat.setSeatName(name);
				seat.setStatus(1);
				seat.setRoom(room);
				seatService.save(seat);
			}
		}
		redirect.addFlashAttribute("success", "Lưu dữ liệu thành công!");
		return "redirect:/rap/phong-chieu";
	}

	@GetMapping("/rap/phong-chieu/{id}/sua-phong-chieu")
	public ModelAndView edit(@PathVariable("id") int roomId, ModelAndView mv, Principal principal) {
		String userName = principal.getName();
		User user = userService.findByUserName(userName);
		mv.addObject("user", user);
		mv.addObject("room", roomService.findOne(roomId));
		mv.setViewName("AddAuditorium");
		return mv;
	}

	@GetMapping("/rap/phong-chieu/{id}/xoa-phong-chieu")
	public String delete(@PathVariable("id") int roomId, RedirectAttributes redirect, Model model, Principal principal) {
		String userName = principal.getName();
		User user = userService.findByUserName(userName);
		model.addAttribute("user", user);
		roomService.delete(roomId);
		redirect.addFlashAttribute("success", "Xóa thành công");
		return "redirect:/rap/phong-chieu";
	}

	private List<String> createSeatName(int seats) {
		List<String> listseatName = new ArrayList<String>();
		char[] alphabet = createAlphabet();
		int k = 0;
		for (int i = 1; i <= seats; i++) {
			StringBuilder temp = new StringBuilder();
			if (i % 10 == 0) {
				temp.append(alphabet[k]).append(10);
				k++;
			} else {
				temp.append(alphabet[k]).append(i % 10);
			}
			listseatName.add(temp.toString());
		}
		return listseatName;
	}

	private char[] createAlphabet() {
		char alphabet[] = new char[26];
		int k = 0;
		for (int i = 65; i <= 90; i++) {
			alphabet[k] = (char) i;
			k++;
		}
		return alphabet;
	}
}
