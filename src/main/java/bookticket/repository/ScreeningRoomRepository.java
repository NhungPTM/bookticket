package bookticket.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import bookticket.entities.ScreeningRoom;

public interface ScreeningRoomRepository extends CrudRepository<ScreeningRoom, Integer>{
	
	@Query(value = "SELECT r.* FROM screening_room r " + 
			"INNER JOIN cinema c ON r.cinema_id = c.cinema_id " + 
			"WHERE c.user_id = ?1", nativeQuery = true)
	List<ScreeningRoom> findByCinema(Integer user_id);
}
