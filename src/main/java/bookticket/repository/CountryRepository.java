package bookticket.repository;

import org.springframework.data.repository.CrudRepository;

import bookticket.entities.Country;

public interface CountryRepository extends CrudRepository<Country, Integer>{

}
