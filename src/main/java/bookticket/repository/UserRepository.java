package bookticket.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import bookticket.entities.User;

public interface UserRepository extends CrudRepository<User, Integer>{
	User findByUserName(String userName);
	User findByEmail(String email);
	User findByPhone(String phone);
	
	@Query(value = "SELECT u.* FROM user_login as u WHERE u.user_id IN"
		        + " (SELECT r.user_id FROM user_role as r WHERE r.role_id = ?1)", nativeQuery = true)
	List<User> findByRole(Integer id);
}
