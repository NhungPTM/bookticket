package bookticket.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import bookticket.entities.ScreeningRoom;
import bookticket.entities.Seat;

public interface SeatRepository extends CrudRepository<Seat, Integer>{

	List<Seat> findByRoom(ScreeningRoom room);
}
