package bookticket.repository;

import org.springframework.data.repository.CrudRepository;

import bookticket.entities.ConfirmationToken;

public interface ConfirmationTokenRepository extends CrudRepository<ConfirmationToken, Integer> {
	ConfirmationToken findByConfirmationToken(String confirmationToken);
}
