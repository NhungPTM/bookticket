package bookticket.repository;

import org.springframework.data.repository.CrudRepository;

import bookticket.entities.Ticket;

public interface TicketRepository extends CrudRepository<Ticket, Integer>{

}
