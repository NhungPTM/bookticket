package bookticket.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import bookticket.entities.Showtimes;

public interface ShowtimeRepository extends CrudRepository<Showtimes, Integer>{
	@Query(value = "SELECT s.* FROM showtime s " + 
			"INNER JOIN screening_room r ON s.room_id = r.room_id " + 
			"INNER JOIN cinema c ON r.cinema_id = c.cinema_id " + 
			"WHERE c.user_id = ?1", nativeQuery = true)
	List<Showtimes> findByCinema(Integer id);
	
	@Query(value = "SELECT s.* FROM cinema c " + 
			"INNER JOIN screening_room r ON c.cinema_id = r.cinema_id " + 
			"INNER JOIN showtime s ON r.room_id = s.room_id " + 
			"INNER JOIN movie m ON s.movie_id = m.movie_id " + 
			"WHERE c.cinema_id = ?1 AND m.movie_id = ?2", nativeQuery = true)
	List<Showtimes> findByCinemaAndMovie(int cinemaId, int movieId);
}
