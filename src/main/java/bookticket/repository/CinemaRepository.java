package bookticket.repository;

import org.springframework.data.repository.CrudRepository;

import bookticket.entities.Cinema;
import bookticket.entities.User;

public interface CinemaRepository extends CrudRepository<Cinema, Integer>{
	Cinema findByUser(User user);
}
