package bookticket.configuration;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	private CustomLoginSuccessHandler sucessHandler;

	@Autowired
	private DataSource dataSource;

	@Value("${spring.queries.users-query}")
	private String usersQuery;

	@Value("${spring.queries.roles-query}")
	private String rolesQuery;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().usersByUsernameQuery(usersQuery).authoritiesByUsernameQuery(rolesQuery)
				.dataSource(dataSource).passwordEncoder(bCryptPasswordEncoder);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests()
				// URLs matching for access rights
				.antMatchers("/").permitAll()
				.antMatchers("/dang-nhap").permitAll()
				.antMatchers("/dang-ky").permitAll()
				.antMatchers("/trang-chu").permitAll()
				.antMatchers("/rap-chieu-phim").permitAll()
				.antMatchers("/xem-lich-chieu").permitAll()
				.antMatchers("/chi-tiet-lich-chieu").permitAll()
				.antMatchers("/phim/**").permitAll()
				.antMatchers("/goc-dien-anh/**").permitAll()
				.antMatchers("//chuong-trinh-thanh-vien").permitAll()
				.antMatchers("/he-thong/**").permitAll()
				.antMatchers("/dat-ve/**").hasAnyAuthority("USER")
				//.antMatchers("/he-thong/**").hasAnyAuthority("BIGADMIN")
				.antMatchers("/rap/**").hasAnyAuthority("ADMIN")
				.anyRequest().authenticated()
				.and()
				// form login
				.csrf().disable().formLogin()
				.loginPage("/dang-nhap")
				.failureUrl("/dang-nhap?error=true")
				.successHandler(sucessHandler)
				.usernameParameter("userName")
				.passwordParameter("password")
				.and()
				// logout
				.logout()
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
				.logoutSuccessUrl("/trang-chu").and()
				.exceptionHandling()
				.accessDeniedPage("/access-denied");
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/resources/**", "/static/**","/bower_components/**", "/assets/**", "/dist/**", "/form/**", "/home/**", "/view/**", "/css/**", "/js/**", "/images/**");
	}

}
